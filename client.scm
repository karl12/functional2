(use-modules (packedobjects packedobjects))
(use-modules (ice-9 pretty-print))
(use-modules (ice-9 match))
(use-modules (ice-9 rdelim))
(use-modules (srfi srfi-1))

(load "net.scm")

;;creates the connection to be used
(define (make-connection)
  (let* ((sock (socket PF_INET SOCK_DGRAM 0))
	 (ipv4addr (inet-pton AF_INET IP))
	 (addr (make-socket-address AF_INET ipv4addr PORT)))
    (connect sock AF_INET ipv4addr PORT)
    `(,sock ,addr)))

;;assigns the connection to the variable conn
(define conn (make-connection))

;;gets fname from search result returned from server
(define (get-fname data)
  (match data
	 [('member lname ('fname fname) . rest)
	  fname]))
;;gets lname from search result returned from server
(define (get-lname data)
  (match data
	 [('member ('lname lname) . rest)
	  lname]))

;;sends and receives query to/from server
(define (query lname op)
  (let ((sock (car conn))
	(addr (cadr conn)))
    ;;sends the message to the server
    (sendmessage sock addr (encode protocol (make-query lname)))
    ;receives the response
    (let ((result (client-match-response
		   (let* ((message (readmessage sock))
			  (pdu (cdr message)))
		     (decode protocol pdu)))))
      (cond
       ;;if the string option -s has been used it will string the
       ;;response, else it will pretty print it
       [(equal? op '-s)
	(let loop [(member result)]
		   (cond
		    [(null? member)]
		    [(= (length member) 1)
		     (member->string (car member))]
		    [else
		     (member->string (car member))
		     (loop (cdr member))]))]
       [else
	(pretty-print result)]))))

;;creates a hash code query as defined in the protocol
(define (make-hash-request hash)
  `(message
    (init-request
     (hash ,hash))))

;;creates a query to be searched for
(define (make-query lname)
  `(message
    (query
     (lname ,lname))))

;;displays the output of the server
(define (client-match-response result)
  (match result
	 [('message
	   ('response
	    ('library)))
	  "nothing found"]
	 [('message
	   ('response
	    ('library members ...)))
	  members]))

;;turns a member return from server into a string
(define (member->string member)
  (let* ((fname (get-fname member))
	 (lname (get-lname member)))
    (format #t "\n~s ~s" fname lname)
    (person->string member)))

;;splits the result, sending each part to the data->string function
(define (person->string member)
  (cond
   [(null? member)]
   [else (data->string (car member))
	  (person->string (cdr member))]))

;;turns individual blocks eg (lname "john") into a string
(define (data->string data)
  (if (not (equal? data 'member))
      (cond
       [(equal? (car data) 'address)
	(format #t "\nlives at ~s" (car (cdr data)))]
       [(eq? (car data) 'postcode)
	(format #t "~s" (car (cdr data)))]
       [(eq? (car data) 'contactnumber)
	(format #t "\nContact number: ~s" (car (cdr data)))]
       [(eq? (car data) 'email)
	(format #t "\nEmail: ~s" (cdr data))])))

;;gets the response from server after sending the hash code
(define (server-get-hash data)
  (match data
	 (('message
	   ('init-response
	    ('confirmation confirmation)))
	  confirmation)))


;;sends hash of protocol to server to be checked
(define (client-match-protocol)
  (let* ((sock (car conn))
	(addr (cadr conn))
	(pdu (encode protocol
		     (make-hash-request (number->string
				       (protocoltohash))))))
    (sendmessage sock addr pdu)
    (let* ((message (readmessage sock))
	   (pdu (cdr message))
	   (confirmation (server-get-hash (decode protocol pdu))))
      (cond
       [(equal? confirmation "matched") 1]
       [else (format #t "Match failed\n") 0]))))

;;first function
(define (start)
  (cond
   [(= (client-match-protocol) 1) (menu)]
   [else (format #t "Protocols do not match")]))


;;menu displayed after checking protocol
(define (menu)
  (format #t "EMA\n")
  (format #t "Commands:\nsearch <term>\nquit\n\n")
  (while #t
	 (format #t "Enter command: ")
	 (let ((input (string-tokenize (read-line))))
	   (if (not (null? input))
	       (let* ((tokens (map string->symbol input))
		      (cmd (car tokens)))
		 (cond
		  ((eq? cmd 'help)
		   (format #t "Commands:\nsearch <term> optional -s string ouput\nquit\n"))
		  ((eq? cmd 'quit)
		   (exit))
		  ((eq? cmd 'search)
		   (pretty-print (last tokens))
		   (cond
		    [(null? (cdr tokens)) 
		     (format #t "No name entered\n")]
		    [else (query (symbol->string (cadr tokens)) (last tokens))]))
		  (else
		   (format #t "Command not recognised\n")
		   (format #t "Commands:\nsearch <term>\nquit\n"))))))))	 
(start)