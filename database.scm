(define database
  '(library
    (member
     (lname "Smith")
     (fname "John")
     (address "21 Main Street")
     (postcode "W3 7UG")
     (contactnumber "02453244")
     (email "johnsmith@hotmail.com"))
    (member
     (lname "Doe")
     (fname "John")
     (address "53 Ealing Broadway")
     (postcode "W5 5TD")
     (contactnumber "05234334")
     (email "johndoe@hotmail.com"))
    (member
     (lname "Doe")
     (fname "Jane")
     (address "53 Ealing Broadway")
     (postcode "W5 5TD")
     (contactnumber "05234334")
     (email "janedoe@hotmail.com"))
    (member
     (lname "Green")
     (fname "Tim")
     (address "21 Hastings Road")
     (postcode "W5 6YT")
     (contactnumber "02257331")
     (email "timgreen@gmail.com"))))