(use-modules (ice-9 match))

(define database
  '(library
    (member
     (id "12321")
     (name "John Smith")
     (address-1st-line "21 Main Street")
     (postcode "w1 3df"))
    (member
     (id "34232")
     (name "John Doe")
     (address-1st-line "12 Pear Street")
     (postcode "w3 3sd"))))

(define (client-make-query name)
  `(message
    (query
     (name ,name))))


(define (server-get-query data)
  (match data
	 (('message
	   ('query
	    ('name name)))
	  name)))

(define (server-make-response keyword)
  `(message
    (response
     (library
      ,@(server-search keyword)))))

(define (server-search keyword)
  (filter
   (lambda (member)
     (match member
	    [('member id ('name name) . rest)
	     (string-prefix-ci? keyword name)]))
   (cdr database)))

(define (client-match-response result)
  (match result
	 [('message
	   ('response
	    ('library)))
	  "no memeber found"]
	 [('message
	   ('response
	    ('library members ...)))
	   members]))

