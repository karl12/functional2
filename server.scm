(use-modules (packedobjects packedobjects))
(use-modules (ice-9 match))
(use-modules (ice-9 pretty-print))

(load "net.scm")
(load "database.scm")

;;gets lname from the query sent by the client
(define (server-get-query data)
  (match data
	 (('message
	   ('query
	    ('lname lname)))
	  lname)))

;;gets the hash code sent by the client
(define (server-get-hash data)
  (match data
	 (('message
	   ('init-request
	    ('hash hash)))
	  hash)))

;;searchs the database using lname from server-get-query
(define (server-search keyword)
  (filter
   (lambda (member)
     (match member
	    [('member ('lname lname) . rest)
	     (string-prefix-ci? keyword lname)]))
   (cdr database)))

(define (server-make-response keyword)
  `(message
    (response
     (library
      ,@(server-search keyword)))))

(define (make-hash-response response)
  `(message
    (init-response
     (confirmation ,response))))

(define (handle-query sock addr query)
  (let ((serverhash (number->string (protocoltohash)))
	(query-type (car (cadr query))))
    (cond
     ;;checks hash
     [(equal? query-type 'init-request) 
      (cond 
       [(equal? serverhash (server-get-hash query))
	;;sends confirmation
	  (sendmessage sock addr 
		       (encode protocol 
			       (make-hash-response "matched")))]
       ;;sends error
       [else
	 (sendmessage sock addr 
		       (encode protocol 
			       (make-hash-response "failed")))])]
     [(equal? query-type 'query)
      (format #t "search for: ~a\n" (server-get-query query))
      (sendmessage sock addr
		   (encode protocol
			   (server-make-response 
			    (server-get-query query))))])))

(define (handle-error key . args)
  (format #t "error: ~a ~a\n" key args))

(define (listen)
  (let ((sock (socket PF_INET SOCK_DGRAM 0)))
    (setsockopt sock SOL_SOCKET SO_REUSEADDR 1)
    (bind sock AF_INET INADDR_ANY PORT)
    (format #t "Listening for clients in pid: ~S\n" (getpid))
    (while #t
	   (let* ((message (readmessage sock))
		  (addr (car message))
		  (pdu (cdr message))
		  (query (decode protocol pdu)))
	     ;;if the hash value of the protocol used by server
	     ;;matchs query, sends confirmation to client
	     (catch #t
		    (lambda ()
		      (handle-query sock addr query))
		    handle-error)))))

(listen)