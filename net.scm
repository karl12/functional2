(use-modules (rnrs bytevectors))

(load "protocol.scm")

(define PORT 2904)
(define MAXPDU 10000)
(define IP "127.0.0.1")

(define recv-buffer (make-bytevector MAXPDU))

;;send message function, used by both client and server
(define (sendmessage sock addr pdu)
  (let* ((size (bytevector-length pdu))
	 (ip (inet-ntop AF_INET (vector-ref addr 1)))
	 (port (vector-ref addr 2)))
    (format #t "tx: ~a bytes to ~a:~a\n" size ip port)
    (sendto sock pdu addr)))

;;receive message function, used by both client and server
(define (readmessage sock)
  (let* ((datagram (recvfrom! sock recv-buffer))
	 (bytes (car datagram))
	 (addr (cdr datagram))
	 (ip (inet-ntop AF_INET (vector-ref addr 1)))
	 (port (vector-ref addr 2)))
    (format #t "rx: ~a bytes from ~a:~a\n" bytes ip port)
    (let ((pdu (make-bytevector bytes)))
      (bytevector-copy! recv-buffer 0 pdu 0 bytes)
      (cons addr pdu))))
