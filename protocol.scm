(define protocol
  `(message choice
	    (init-request sequence
			  (hash numeric-string (size 1 15)))
	    (init-response sequence
			   (confirmation string (size 1 15)))	     
	    (query sequence
		   (lname string (size 1 15)))
	    (response sequence
		      (library sequence-of
			       (member sequence-optional
				       (lname string (size 1 15))      	    
				       (fname string (size 1 15))
				       (address string (size 1 100))
				       (postcode string (size 5 6))
				       (contactnumber string (size 7 10))
				       (email string (size 5 100)))))))

;;turns protocol in to a hash value
(define (protocoltohash)
  ;;loops through protocol turning each symbol to a a hash
  ;;and adding the integers
  (let loop [(proto protocol)
	      (n 0)]
	     (cond
	      [(null? (car proto)) n]
	      [(symbol? (car proto))
	       (+ n (symbol-hash (car proto)))]
	      [(integer? (car proto))
	       (+ n (car proto))]
	      [else
	       (loop (cdr proto) n)
	       (loop (car proto) n)])))